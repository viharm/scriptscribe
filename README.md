# ScriptScribe

|           |                                                                   |
|:----------|:------------------------------------------------------------------|
| Version   | 1.0.0                                                             |
| Changes   | https://gitlab.com/viharm/scriptscribe/-/merge_requests           |
| Download  | https://gitlab.com/viharm/scriptscribe/-/tags                     |
| Issues    | https://gitlab.com/viharm/scriptscribe/-/issues                   |
| License   | Apache 2.0                                                        |
| Language  | Shell                                                             |


## Introduction 

_ScriptScribe_ is a _POSIX_-compliant shell script library which provides logging functionality for any shell script.

It uses [RFC3164](https://datatracker.ietf.org/doc/html/rfc3164#section-4.1.1) logging levels, as follows

| Logging level |    Title     |                 Description                  |
|--------------:|:-------------|:---------------------------------------------|
|            `0`|Emergency     |System is unusable                            |
|            `1`|Alert         |Action must be taken immediately              |
|            `2`|Critical      |Critical condition                            |
|            `3`|Error         |Error condition                               |
|            `4`|Warning       |Warning condition (expect unusual behaviour)  |
|            `5`|Notice        |Normal but significant condition              |
|            `6`|Information   |Informational message                         |
|            `7`|Debug         |Debug level message                           |


## Features

_ScriptScribe_ provides the following optional functionality.

  - Logging to console (default)
  - Logging to syslog instead of console
  - Logging to file instead of console
  - Compress the log file using _GZip_, _BZip2_ or _XZip_ (if the tools exist on the system)


## Installation


### Pre-requisites

The library is fully contained in the file `ScriptScribe.sh`.

It requires a typical suite of POSIX utilities.

Additionally, if the optional compression of the log file is required, then the relevant compression tools must be installed and available in the system path.


### Download

Download the library either as a package or clone it.


#### Archive

Get the release archives from the download link provided at the top of this page.


#### Clone repository

Clone the repository into any preferred location within the context of your program/code.
```
git clone --recurse-submodules \
https://gitlab.com/viharm/scriptscribe.git \
/PATH/TO/YOURPROGRAM/source/
```
(remember to change the sub-directory `source` to your chosen location for libraries)


## Basic Usage

Include the library in your shell script, using the `fn_LoggingInit` command
```
. $(dirname $(readlink -f $0))/source/ScriptScribe.sh
```
(remember to change the sub-directory `source` to your chosen location for libraries)


### Initialize logging

Initialize the logging library with a basic context. The minimum information required is a numeric logging level.

```
fn_LoggingInit 5
```
This will setup the calling script at a logging level of `5` (Notice). This means all messages of level 5 or below will be logged.


### Log messages

Log messages using the `fn_Log` command, with two arguments - the numeric log level of the message and the message itself.
```
fn_Log 5 "This is a notice"
```
If the level of the message is lower than or equal to the script logging level, then it will be logged.


### Features and advanced usage


#### Initialization


##### Setup logging to syslog

To log to _syslog_ instead of the console, simply pass `s` as the second argument when initializing.

```
fn_LoggingInit 6 s
```


##### Setup logging to file

To log to a file instead of the console, simply pass the path of the file as the third argument.

|                                                                                |
|:------------------------------------------------------------------------------:|
| **Note that the second argument should be any non-null string other than `s`** |



```
fn_LoggingInit 6 anything /path/to/log/file.txt
```


##### Compressing log file after completion

To compress the log file after the script as finished running, two steps have to be carried out:
  1. Initialize the library by providing the compression format as the fourth argument. This could be one of the following:
    a. `gz` for GZip compression
    a. `bz2` for BZip2 compression
    a. `xz` for XZip compression
  1. Issue a command at the end of your script to compress the log file - `fn_CompressLogfile`

|                                                                                         |
|:---------------------------------------------------------------------------------------:|
| **Note that the appropriate comrpession binaries must be available in the system path** |
|     **Note that _ScriptScribe_ will use default options to compress the log file**      |

```
fn_LoggingInit 3 anything /path/to/log/file.txt gz
...
YOUR SCRIPT HERE

...
fn_CompressLogfile
```

You could also include this command in your cleanup function
```
fn_Finish() {
  printf "Cleaning up..."
  fn_CompressLogfile
  exit $nm_ScriptErrorState
}

trap fn_Finish 0 1 2 5 15
```


## Known limitations

Limitations of _ScriptScribe_ are listed in the issues linke at the top of this page.


## Support

For more information on installation, configuration, additional options; or if you have suggestions or comments, please create an issue at the link provided at the top of this page.


## Contribute

Please feel free to clone/fork and contribute via pull requests. Donations also welcome, simply create an issue at the link provided at the top of this page.

Please make contact for more information.


## Environment ##
Platform and software stack known to be compatible:

* OS
    * _Debian Buster_
    * _Ubuntu 20.04_
* Shell
    * _Bash_


## License

Copyright (C) MMXXII viharm

Licensed under the Apache License, Version 2.0 (the "License"); you may not use any file in this project/package except in compliance with the License.

You should have received a copy of the License along with this program.  If not, see <https://www.apache.org/licenses/LICENSE-2.0>.

See enclosed [`LICENSE`](LICENSE?at=master) file.


## References

  1. Logging levels: https://datatracker.ietf.org/doc/html/rfc3164#section-4.1.1
  1. Console text formatting: https://martin-thoma.com/colorize-your-scripts-output
  1. Colorizing logic: https://github.com/swelljoe/slog


## Credits


### Utilities


#### VS Code

*Visual Studio Code* code editor, used under the *Microsoft Software License*.


#### Kubuntu

_Kubuntu_ operating system used under the GNU GPL version 2 or later.

Copyright (C) Canonical Ltd.


#### GitLab

Hosted by *GitLab* code repository (gitlab.com).


#### License

Licensing guidance provided by [Opensource.org](https://opensource.org)

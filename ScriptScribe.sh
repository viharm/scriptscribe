#!/usr/bin/sh


  # ===== How to use =====

  # Include in your script with a dot
  # The following example assumes that the library is in the same directory/folder as your script.
    # . $(dirname $(readlink -f $0))/ScriptScribe.sh

  # Initialize your script global logging level
    # fn_LoggingInit 7

  # Optionally choose logging to syslog
    # fn_LoggingInit 7 s

  # Optionally choose logging to file
    # fn_LoggingInit 7 n /path/to/log/file

  # Optionally choose to compress the log file
    # fn_LoggingInit 7 n /path/to/log/file gz


  # ===== Initialize script variables =====

  nm_ScriptErrorState=$((0))
  sr_DateBin=$(command -v date)
  sr_LoggerBin=$(command -v logger)
  sr_LoggerOption1="-i -s -p local6.info"
  sr_LoggerOption2=" -t "$(basename "$0")
  sr_EvalBin=$(command -v eval)
  sr_TputBin=$(command -v tput)


  # ===== Define functions =====


  # ==== Function to format date ====

  # A separate function is needed due to the space in the date format [https://unix.stackexchange.com/a/230466]
  fn_DateCommand() {
    $sr_DateBin '+%Y-%m-%d %H:%M:%S'
  }

  # ==== Function to map numeric logging level to descriptive logging level ====

  fn_LogLevelDict() {
    
    nm_LogLevelDict_FnErrorState=$((1))
    nm_LogLevelDict_ArgCount=$#

    if [ $nm_LogLevelDict_ArgCount -gt 0 ]; then
      nm_LogLevelDict_Input=$1
      if [ $nm_LogLevelDict_Input -eq $nm_LogLevelDict_Input 2> /dev/null ]; then
        nm_LogLevel=$(($nm_LogLevelDict_Input))
        case $nm_LogLevelDict_Input in
          0) sr_LogLevel="Emergency";;               # System is unusable
          1) sr_LogLevel="Alert";;                   # Action must be taken immediately
          2) sr_LogLevel="Critical";;                # Critical condition
          3) sr_LogLevel="Error";;                   # Error condition
          4) sr_LogLevel="Warning";;                 # Warning condition (expect unusual behaviour)
          5) sr_LogLevel="Notice";;                  # Normal but significant condition
          6) sr_LogLevel="Information";;             # Informational message
          7) sr_LogLevel="Debug";;                   # Debug level message
          *) sr_LogLevel="Unknown logging level";;   # Unspecified
        esac
        nm_LogLevelDict_FnErrorState=$((0))
      else
        echo "[Warning]: Log level should be numeric"
      fi
    else
      echo "[Warning]: Log level request not found"
    fi

    unset nm_LogLevelDict_ArgCount
    unset nm_LogLevelDict_Input
    echo $sr_LogLevel 2> /dev/null
    return $nm_LogLevelDict_FnErrorState
  }


  # ==== Function to set script logging level ====

  fn_SetScriptLogLevel() {

    nm_SetScriptLogLevel_FnErrorState=$((1))
    nm_SetScriptLogLevel_ArgCount=$#

    if [ -z $nm_ScriptLogLevel ]; then
      nm_ScriptLogLevel=$((4))
    fi

    if [ $nm_SetScriptLogLevel_ArgCount -gt 0 ]; then
      nm_SetScriptLogLevel_Input=$1
      if [ $nm_SetScriptLogLevel_Input -eq $nm_SetScriptLogLevel_Input 2> /dev/null ]; then
        if [ $nm_SetScriptLogLevel_Input -lt 0 -o $nm_SetScriptLogLevel_Input -gt 7 ]; then
          echo "[Warning]: Log level should be between 0 & 7, inclusive"
        else
          nm_ScriptLogLevel=$(($nm_SetScriptLogLevel_Input))
          # sr_ScriptLogLevel=$(fn_LogLevelDict $nm_ScriptLogLevel)
          nm_SetScriptLogLevel_FnErrorState=$((0))
        fi
      else
        echo "[Warning]: Script log level should be a numeric value."
      fi
    else
      echo "[Warning]: Script logging level should be provided as an argument"
    fi

    sr_ScriptLogLevel=$(fn_LogLevelDict $nm_ScriptLogLevel)

    if [ $(($nm_SetScriptLogLevel_FnErrorState)) -ne 0 ]; then
      # nm_ScriptLogLevel=$((4))
      echo "[Warning]: Failed to change script logging level. Logging level left unchanged at $nm_ScriptLogLevel ($(fn_LogLevelDict $nm_ScriptLogLevel))."
    fi

    unset nm_SetScriptLogLevel_Input
    unset nm_SetScriptLogLevel_ArgCount
    return $(($nm_SetScriptLogLevel_FnErrorState))
  }


  # ==== Function to enable or disable logging output to syslog ====

  # Set debugging-to-syslog mode; 0: normal (default), 1: debug to syslog
  fn_SetSyslogOutput() {

    nm_SetSyslogOutput_FnErrorState=$((1))
    nm_SetSyslogOutput_ArgCount=$#

    if [ -z $bl_Syslog ]; then
      bl_Syslog=$((0))
    fi

    if [ $nm_SetSyslogOutput_ArgCount -gt 0 ]; then
      nm_SetSyslogOutput_Input=$1
      if [ $nm_SetSyslogOutput_Input -eq $nm_SetSyslogOutput_Input 2> /dev/null ]; then
        if [ $nm_SetSyslogOutput_Input -eq 0 -o $nm_SetSyslogOutput_Input -eq 1 ]; then
          bl_Syslog=$(($nm_SetSyslogOutput_Input))
          nm_SetSyslogOutput_FnErrorState=$((0))
        fi
      fi
    fi

    if [ $(($nm_SetSyslogOutput_FnErrorState)) -ne 0 ]; then
      echo "[Warning]: Syslog request should be either 0 or 1. Failed to change syslog output state. Syslog output state left unchanged at $bl_Syslog."
    fi

    unset nm_SetSyslogOutput_ArgCount
    unset nm_SetSyslogOutput_Input
    return $(($nm_SetSyslogOutput_FnErrorState))
  }


  # ==== Function to setup log file ====
  
  fn_SetupLogFile () {
  
    nm_SetupLogFile_FnErrorState=$((1))
    nm_SetupLogFile_ArgCount=$#

    if [ $nm_SetupLogFile_ArgCount -gt 0 ]; then
      sr_SetupLogFile_Input=$1
      if [ -e "$sr_SetupLogFile_Input" ]; then
        if [ -f "$sr_SetupLogFile_Input" ]; then
          if [ -w "$sr_SetupLogFile_Input" ]; then
            sr_LogfilePath=$sr_SetupLogFile_Input
            nm_SetupLogFile_FnErrorState=$((0))
          else
            echo "[Warning]: $sr_SetupLogFile_Input is not a writeable file. Will not log to file"
          fi
        else
          echo "[Warning]: $sr_SetupLogFile_Input is not a regular file. Will not log to file"
        fi
      else
        $(command -v touch) "$sr_SetupLogFile_Input"
        if [ $(($?)) -ne 0 ]; then
          echo "[Warning]: Could not create log file at $sr_SetupLogFile_Input. Will not log to file"
        else
          sr_LogfilePath=$sr_SetupLogFile_Input
          nm_SetupLogFile_FnErrorState=$((0))
        fi
      fi
    else
      echo "[Warning]: Log file path not provided. Will not log to file."
    fi

      unset sr_SetupLogFile_Input
      unset nm_SetupLogFile_ArgCount
      return $nm_SetupLogFile_FnErrorState
  }


  # ==== Function to configure log file compression ====
  
  fn_SetLogfileCompression() {
  
    nm_SetLogfileCompression_FnErrorState=$((1))
    nm_SetLogfileCompression_ArgCount=$#
    sr_SetLogfileCompression_Input=$(echo $1 | tr '[:lower:]' '[:upper:]')

    if [ $nm_SetLogfileCompression_ArgCount -gt 0 ]; then
      case $sr_SetLogfileCompression_Input in
        "GZ") sr_LogfileCompressFormat="gzip" && sr_LogfileCompressedExtn="gz" && nm_SetLogfileCompression_FnErrorState=$((0)) ;;
        "XZ") sr_LogfileCompressFormat="xz" && sr_LogfileCompressedExtn="xz" && nm_SetLogfileCompression_FnErrorState=$((0)) ;;
        "BZ2") sr_LogfileCompressFormat="bzip2" && sr_LogfileCompressedExtn="bz2" && nm_SetLogfileCompression_FnErrorState=$((0)) ;;
        "TESTCOMPRESSOR") sr_LogfileCompressFormat="testcompressor" && nm_SetLogfileCompression_FnErrorState=$((0)) ;;
        *) echo "[Warning]: Log file compression must be one of \"GZ\", \"XZ\" or \"BZ2\". Will not compress log file." ;;
      esac
    else
      echo "[Warning]: Log file compression not specified. Will not compress log file."
    fi

    if [ $nm_SetLogfileCompression_FnErrorState -eq 0 ]; then
      sr_CompressorBin=$(command -v $sr_LogfileCompressFormat)

      if [ DUMMYTEXT$sr_CompressorBin = "DUMMYTEXT" ]; then
        nm_SetLogfileCompression_FnErrorState=$((1))
        echo "[Warning]: $sr_LogfileCompressFormat not found. Will not compress log file."
      fi
    fi
    unset nm_SetLogfileCompression_ArgCount
    unset sr_SetLogfileCompression_Input
    return $nm_SetLogfileCompression_FnErrorState
  }
  
  # ==== Function to compress log file ====
  
  fn_CompressLogfile() {
  
    nm_CompressLogfile_FnErrorState=$((1))
    
    if [ -z $nm_SetLogfileCompression_FnErrorState ]; then
      nm_SetLogfileCompression_FnErrorState=$((1))
    fi

    if [ $nm_SetLogfileCompression_FnErrorState -eq 0 ]; then
      $sr_CompressorBin "$sr_LogfilePath"
      if [ $? -ne 0 ]; then
        echo "[Warning]: Failed to compress log file."
      else
        nm_CompressLogfile_FnErrorState=$((0))
      fi
    else
      echo "[Warning]: Log file compression not configured properly"
    fi

    return $nm_CompressLogfile_FnErrorState
  }


  # ==== Function to initialize logging behaviour ====

  fn_LoggingInit() {

    nm_LoggingInit_FnErrorState=$((1))
    nm_LoggingInit_ArgCount=$#

    vr_TextFormat_0=""
    vr_TextFormat_1=""
    vr_TextFormat_2=""
    vr_TextFormat_3=""
    vr_TextFormat_4=""
    vr_TextFormat_5=""
    vr_TextFormat_6=""
    vr_TextFormat_7=""
    vr_TextFormat_Def=""

    if [ "DUMMYTEXT$sr_TputBin" != "DUMMYTEXT" ]; then
      if [ $(tty -s) ]; then
        readonly bl_InteractiveMode=$((0))
      else
        readonly bl_InteractiveMode=$((1))
      fi

      if [ "${bl_InteractiveMode}" -eq 1 ]; then
        vr_TextFormat_0=$($sr_TputBin setaf 1)$($sr_TputBin bold)  # Bold red
        vr_TextFormat_1=$($sr_TputBin setaf 1)$($sr_TputBin bold)  # Bold red
        vr_TextFormat_2=$($sr_TputBin setaf 1)                     # Red
        vr_TextFormat_3=$($sr_TputBin setaf 1)                     # Red
        vr_TextFormat_4=$($sr_TputBin setaf 3)                     # Yellow / amber
        vr_TextFormat_5=$($sr_TputBin setaf 6)                     # Cyan
        vr_TextFormat_6=$($sr_TputBin sgr0)                        # Normal
        vr_TextFormat_7=$($sr_TputBin setaf 7)                     # Grey
        vr_TextFormat_Def=$($sr_TputBin sgr0)                      # Normal
      fi
    fi

    if [ $nm_LoggingInit_ArgCount -lt 1 ]; then
      echo "[Error]: Need at least one argument to initialize logging"
    else
      fn_SetScriptLogLevel $1
      if [ $(($?)) -ne 0 ]; then
        nm_LoggingInit_FnErrorState=$((1))
      fi

      if [ $nm_LoggingInit_ArgCount -gt 1 ]; then
        sr_LogOutput="$2"
        if [ $sr_LogOutput = "s" ]; then
          fn_SetSyslogOutput 1
        fi
      fi

      if [ $nm_LoggingInit_ArgCount -gt 2 ]; then
        fn_SetupLogFile $3
      fi
      
      if [ $nm_LoggingInit_ArgCount -gt 3 ]; then
        fn_SetLogfileCompression $4
      fi
    fi

    unset nm_LoggingInit_ArgCount
    return $nm_LoggingInit_FnErrorState
  }


  # ==== Function to log a statement ====

  fn_Log() {

    nm_Log_FnErrorState=$((1))
    nm_Log_ArgCount=$#

    if [ $nm_Log_ArgCount -lt 2 ]; then
      echo "[Error]: Need at least two arguments to log"
    else
      nm_RequestLogLevel=$1
      sr_LogStatement=$2
      
      if [ -z $nm_SetupLogFile_FnErrorState ]; then
        nm_SetupLogFile_FnErrorState=$((1))
      fi

      if [ -n sr_LogStatement ]; then
        if [ $nm_RequestLogLevel -ge 0 -a $nm_RequestLogLevel -le 7 ]; then
          if [ $nm_RequestLogLevel -le $nm_ScriptLogLevel ]; then
            sr_RequestLogLevel=$(fn_LogLevelDict $nm_RequestLogLevel)
            $sr_EvalBin sr_RequestLogTextFormat="\$vr_TextFormat_$nm_RequestLogLevel"
            if [ $(($bl_Syslog)) -eq 1 ]; then
              $sr_LoggerBin $sr_LoggerOption1 "[$sr_RequestLogLevel]: $sr_LogStatement" $sr_LoggerOption2
              # if this is selected then shell default behaviour may be to output to console too;
              if [ $(($?)) -eq 0 ]; then
                nm_Log_FnErrorState=$((0))
              fi
            elif [ $nm_SetupLogFile_FnErrorState -eq 0 ]; then
              echo "$(fn_DateCommand) [$sr_RequestLogLevel]: $sr_LogStatement" >> "$sr_LogfilePath"
              if [ $(($?)) -eq 0 ]; then
                nm_Log_FnErrorState=$((0))
              fi
            else
              echo "$sr_RequestLogTextFormat$(fn_DateCommand) [$sr_RequestLogLevel]: $sr_LogStatement$vr_TextFormat_Def"
              if [ $(($?)) -eq 0 ]; then
                nm_Log_FnErrorState=$((0))
              fi
            fi
          fi
        else
          echo "[Error]: Log level should be between 0 & 7, inclusive"
        fi
      else
        echo "[Error]: No log statement provided"
      fi
    fi
    
    return $nm_Log_FnErrorState
  }


  # ===== References =====

  # Logging levels: https://datatracker.ietf.org/doc/html/rfc3164#section-4.1.1
  # Console text formatting: https://martin-thoma.com/colorize-your-scripts-output
  # Colorizing logic: https://github.com/swelljoe/slog
